﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : MonoBehaviour {

	public Sprite itemSprite;
	public string itemName;

	// Cost as if buying from the buy menu in an rpg
	public int buyValue;

	// Cost as if selling from the sell menu in an rpg.
	public int sellValue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
