﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour {

	public int day;
	private Text dayText;
	public int money;
	private Text moneyText;
	public List<InventoryItem> inventory;

	// Use this for initialization
	void Start () {
		day = 20;
		money = 1000;
		GameObject dayObj = GameObject.FindGameObjectWithTag ("Day");
		dayText = dayObj.GetComponent<Text> ();
		GameObject moneyObj = GameObject.FindGameObjectWithTag ("Money");
		moneyText = moneyObj.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate() {
		dayText.text = "Day " + day;
		moneyText.text = GetMoneyString (money);
	}

	private string GetMoneyString(int monies) {
		string moneyString = "" + monies;
		int numOfLeadingZeroes = 7 - moneyString.Length;
		for (int i = 0; i < numOfLeadingZeroes; i++) {
			moneyString = "0" + moneyString;
		}
		return moneyString;
	}
}
