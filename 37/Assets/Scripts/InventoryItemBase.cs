﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemBase : MonoBehaviour {

	private GameState gameState;
	private Sprite sprite;

	public string itemName;

	// Cost as if buying from the buy menu in an rpg
	public int buyValue;

	// Cost as if selling from the sell menu in an rpg.
	public int sellValue;

	// Use this for initialization
	void Start () {
		GameObject gameStateObj = GameObject.FindGameObjectWithTag ("GameState");
		gameState = gameStateObj.GetComponent<GameState> ();
		sprite = gameObject.GetComponent<Sprite> ();
	}

	public void AddToInventory(){
		InventoryItem item = new InventoryItem ();
		item.itemSprite = sprite;
		item.itemName = itemName;
		item.buyValue = buyValue;
		item.sellValue = sellValue;
		gameState.inventory.Add (item);
	}
}
